;;; lint-openapi-checker.el --- Flycheck checker with lint-openapi -*- lexical-binding: t; -*-

;; Copyright (C) 2021 Alex Brick

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;; 
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; Author: Alex Brick <alex@alexbrick.me>
;; Version: 1.0.0
;; Package-Requires: ((flycheck "26") seq)
;; URL: https://gitlab.com/bricka/lint-openapi-checker

;;; Commentary:

;;; Code:

(defun lint-openapi-checker--partition-output (output)
  "Split OUTPUT into two separate sets of lines: one before the JSON output, and one after.  Returns the result as a cons cell."
  (let* ((lines (split-string output "\n"))
         (json-start-idx
          (seq-position lines nil (lambda (line _) (string-prefix-p "{" line)))))
    (if json-start-idx
        (cons (seq-subseq lines 0 json-start-idx) (seq-subseq lines json-start-idx))
      (cons lines nil))
    )
  )

(defun lint-openapi-checker--construct-flycheck-error (buffer level err)
  "Constructs the Flycheck error for ERR with level LEVEL (either 'warning' or 'error') in buffer BUFFER."
  (flycheck-error-new
   :buffer buffer
   :checker 'lint-openapi
   :filename (buffer-file-name buffer)
   :id (alist-get 'rule err)
   :level level
   :line (alist-get 'line err)
   :message (alist-get 'message err)
   )
  )

(defun lint-openapi-checker--convert-section-to-errors (buffer level section)
  "Convert the given SECTION to Flycheck errors at level LEVEL for BUFFER."
  (flatten-tree
   (seq-map
    (lambda (area)
      (seq-map
       (lambda (err) (lint-openapi-checker--construct-flycheck-error buffer level err))
       (cdr area)
       )
      )
    section
    )
   )
  )

(defun lint-openapi-checker--parse-output (output _checker buffer)
  "Parse the OUTPUT of lint-openapi and return Flycheck error objects for BUFFER."
  (let ((lines (lint-openapi-checker--partition-output output)))
    (when (cdr lines)
      (let* ((parsed-output
              (car (flycheck-parse-json (string-join (cdr lines) "\n"))))
             (warnings (alist-get 'warnings parsed-output))
             (errors (alist-get 'errors parsed-output))
             )
        (seq-concatenate
         'list
         (lint-openapi-checker--convert-section-to-errors buffer 'warning warnings)
         (lint-openapi-checker--convert-section-to-errors buffer 'error errors)
         )
        ))))

(defun lint-openapi-checker--predicate ()
  "Determine if the current buffer is valid for this checker."
  (equal "openapi" (file-name-base buffer-file-name))
  )

(flycheck-def-config-file-var flycheck-lint-openapi-validaterc lint-openapi ".validaterc")

(flycheck-define-checker lint-openapi
  "OpenAPI linter using lint-openapi."
  :command ("lint-openapi" "-j" (config-file "--config" flycheck-lint-openapi-validaterc) source)
  :error-parser lint-openapi-checker--parse-output
  :modes yaml-mode
  :predicate lint-openapi-checker--predicate
  )

(add-to-list 'flycheck-checkers 'lint-openapi)

(provide 'lint-openapi-checker)
;;; lint-openapi-checker ends here
